
**مقدمه** 

در سال ۱۹۶۷ مارتین ریچاردز زبان BCPL را برای نوشتن نرم‌افزارهای سیستم‌عامل و کامپایلر در دانشگاه کمبریج ابداع کرد. سپس در سال ۱۹۷۰ کن تامسون بل زبان B را بر مبنای ویژگی‌های زبان BCPL نوشت و از آن برای ایجاد اولین نسخه‌های سیستم‌عامل یونیکس در آزمایشگاه‌های بل استفاده کرد. زبان C در سال ۱۹۷۲ توسط دنیس ریچی از روی زبان B و BCPL در آزمایشگاه بل ساخته شد و ویژگی‌های جدیدی همچون نظارت بر نوع داده‌ها نیز به آن اضافه شد. ریچی از این زبان برای ایجاد سیستم‌عامل یونیکس استفاده کرد اما بعدها اکثر سیستم‌عامل‌های دیگر نیز با همین زبان نوشته شدند. این زبان با سرعت بسیاری گسترش یافت و چاپ کتاب "The C Programming Language" در سال ۱۹۷۸ توسط برایان کرنیگان و ریچی باعث رشد روزافزون این زبان در جهان شد.

متأسفانه استفاده گسترده این زبان در انواع کامپیوترها و سخت‌افزارهای مختلف باعث شد که نسخه‌های مختلفی از این زبان بوجود آید که با یکدیگر ناسازگار بودند. در سال ۱۹۸۳ مؤسسه استانداردهای ملی آمریکا (ANSI) کمیته‌ای موسوم به X3J11 را را مأمور کرد تا یک تعریف فاقد ابهام و مستقل از ماشین را از این زبان تدوین نماید. در سال ۱۹۸۹ این استاندارد تحت عنوان ANSI C به تصویب رسید و سپس در سال ۱۹۹۰، سازمان بین‌المللی استانداردسازی (ISO) نیز این استاندارد را پذیرفت و مستندات مشترک آن‌ها تحت عنوان ANSI/ISO C منتشر گردید.

در سال‌های بعد و با ظهور روش‌های برنامه‌نویسی شئ‌گرا نسخه جدیدی از زبان C بنام C++ توسط بی‌یارنه استراس‌تروپ در اوایل ۱۹۸۰ در آزمایشگاه‌های بل توسعه یافت. در C++ علاوه بر امکانات جدید، خاصیت شئ‌گرایی نیز به C اضافه شده‌است.

با گسترش شبکه و اینترنت، نیاز به زبانی احساس شد که برنامه‌های آن بتوانند بر روی هر ماشین و هر سیستم‌عامل دلخواهی اجرا گردد. شرکت سان مایکروسیستمز در سال ۱۹۹۵ میلادی زبان جاوا را برمبنای C و C++ ایجاد کرد که هم‌اکنون از آن در سطح وسیعی استفاده می‌شود و برنامه‌های نوشته شده به آن بر روی هر کامپیوتری که از جاوا پشتیبانی کند (تقریباً تمام سیستم‌های شناخته شده) قابل اجرا می‌باشد. 

**برنامه‌نویسی ساختار یافته**

در دهه ۱۹۶۰ میلادی توسعه نرم‌افزار دچار مشکلات عدیده‌ای شد. در آن زمان سبک خاصی برای برنامه‌نویسی وجود نداشت و برنامه‌ها بدون هیچگونه ساختار خاصی نوشته می‌شدند. وجود دستور پرش (goto) نیز مشکلات بسیاری را برای فهم و درک برنامه توسط افراد دیگر ایجاد می‌کرد، چرا که جریان اجرای برنامه مرتباً دچار تغییر جهت شده و دنبال کردن آن دشوار می‌گردید؛ لذا نوشتن برنامه‌ها عملی بسیار زمان بر و پرهزینه شده بود و معمولاً اشکال زدایی، اعمال تغییرات و گسترش برنامه‌ها بسیار مشکل بود. فعالیت‌های پژوهشی در این دهه باعث بوجود آمدن سبک جدیدی از برنامه‌نویسی بنام برنامه‌نویسی ساخت‌یافته گردید؛ روش منظمی که باعث ایجاد برنامه‌هایی کاملاً واضح و خوانا گردید که اشکال‌زدایی و خطایابی آن‌ها نیز بسیار ساده‌تر بود.

اصلی‌ترین نکته در این روش عدم استفاده از دستور پرش (goto) است. تحقیقات بوهم و ژاکوپینی نشان داد که می‌توان هر برنامه‌ای را بدون دستور پرش و فقط با استفاده از ۳ ساختار کنترلی ترتیب، انتخاب و تکرار نوشت.

ساختار ترتیب، همان اجرای دستورات به صورت متوالی (یکی پس از دیگری) است که کلیه زبان‌های برنامه‌نویسی در حالت عادی بهمان صورت عمل می‌کنند.

ساختار انتخاب به برنامه‌نویس اجازه می‌دهد که براساس درستی یا نادرستی یک شرط، تصمیم بگیرد کدام مجموعه از دستورات اجرا شود.

ساختار تکرار نیز به برنامه نویسان اجازه می‌دهد مجموعه خاصی از دستورات را تا زمانی‌که شرط خاصی برقرار باشد، تکرار نماید.

هر برنامه ساخت‌یافته از تعدادی بلوک تشکیل می‌شود که این بلوک‌ها به ترتیب اجرا می‌شوند تا برنامه خاتمه یابد (ساختار ترتیب). هر بلوک می‌تواند یک دستور ساده مانند خواندن، نوشتن یا تخصیص مقدار به یک متغیر باشد یا اینکه شامل دستورهایی باشد که یکی از ۳ ساختار فوق را پیاده‌سازی کنند. نکته مهم اینجاست که درمورد دستورات داخل هر بلوک نیز همین قوانین برقرار است و این دستورات می‌توانند از تعدادی بلوک به شرح فوق ایجاد شوند و تشکیل ساختارهایی مانند حلقه‌های تودرتو را دهند.

نکته مهم اینجاست که طبق قوانین فوق یک حلقه تکرار یا بطور کامل داخل حلقه تکرار دیگر است یا بطور کامل خارج آن قرار می‌گیرد و هیچگاه حلقه‌های روی هم افتاده نخواهیم داشت.

از جمله اولین تلاشها در زمینه ساخت زبان‌های برنامه‌نویسی ساخت‌یافته، زبان پاسکال بود که توسط پروفسور نیکلاوس ویرت در سال ۱۹۷۱ برای آموزش برنامه‌نویسی ساخت یافته در محیط‌های آموزشی ساخته شد و بسرعت در دانشگاه‌ها رواج یافت اما به دلیل نداشتن بسیاری از ویژگی‌های مورد نیاز مراکز صنعتی و تجاری در بیرون دانشگاه‌ها موفقیتی نیافت.

کمی‌بعد زبان C ارائه گردید که علاوه بر دارا بودن ویژگی‌های برنامه‌نویسی ساخت یافته به دلیل سرعت و کارایی بالا مقبولیتی همه گیر یافت و هم‌اکنون سالهاست که به‌عنوان بزرگ‌ترین زبان برنامه‌نویسی دنیا شناخته شده‌است. 
